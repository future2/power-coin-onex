pragma solidity ^0.4.0;

// This contract tracks the number of kWh transacted per day, with an initial
// balance of 1MWh and is depleted or topped up each time a buy or sell request
// has been initiated. Seller outstanding amounts are settled at the end of the 
// through Ethereum payments.

// unitSellPrice = Price PowerCoin sells to consumer
// unitBuyPrice = Price PowerCoin buys from grid

contract CurrentMarket {
    uint public unitBalance;
    uint public unitSellPrice;
    uint public unitBuyPrice;
    address public issuer;
    address public seller;
    
    mapping(address => uint) sellerSettlements;
    
    // Determine the balance, unit price and contract creator
    function CurrentMarket (uint sellingPrice, uint buyingPrice) {
        unitBalance = 1000;
        unitSellPrice = sellingPrice;
        unitBuyPrice = buyingPrice;
        issuer = msg.sender;
    }
    
    // Check for issuer
    modifier ifIssuer(){
        if (issuer != msg.sender) {
            throw;
        }
        _;
            
    }
    
    // Check contract balance
    function checkBalance () ifIssuer constant returns(uint) {
        return this.balance;
    }
        
    // Sell power to purchasers, returns true when payment is sufficient
    // and false when insufficient amount was paid
    function purchasePower (uint buyUnits) payable returns(bool) {
        uint collectableSum = unitSellPrice * buyUnits;
        
        if (collectableSum <= msg.value) {
            unitBalance = unitBalance - buyUnits;
            return true;
        }
        else {
            return false;
        }
    }
    
    // Purchase power from sellers at the predetermined price
    // as long as it's below the contract set price, and sold at contract
    // selling price if higher
    function sellPower (uint sellUnits, uint sellPrice) returns(bool) {
        uint payableSum;
        
        seller = msg.sender;
        
        if (sellPrice >= unitBuyPrice) {
            payableSum = unitBuyPrice * sellUnits;
        }
        else {
            payableSum = sellPrice * sellUnits;
        }
        
        if (payableSum <= this.balance) {
            seller.transfer(payableSum);
            unitBalance = unitBalance + sellUnits;
            return true;
        }
        else {
            // Still need to write this area to refund seller money on settlement
            return false;
        }
    }
    
    // Still a work in progress
    function performSettlement () returns(bool) {
        var amount = sellerSettlements[msg.sender];
        if (amount > 0) {
            // It is important to set this to zero because the recipient
            // can call this function again as part of the receiving call
            // before `send` returns (see the remark above about
            // conditions -> effects -> interaction).
            sellerSettlements[msg.sender] = 0;

            if (!msg.sender.send(amount)){
                // No need to call throw here, just reset the amount owing
                sellerSettlements[msg.sender] = amount;
                return false;
            }
        }
    }
}